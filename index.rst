.. raw:: html

   <div id="navbar-top" class="navbar navbar-fixed-top navbar-default"
   role="navigation" aria-label="top navigation">

.. raw:: html

   <div class="container-fluid">

.. raw:: html

   <div class="navbar-header">

Toggle navigation
`Mouse\_TCR 1 documentation <#>`__

.. raw:: html

   </div>

.. raw:: html

   <div class="navbar-collapse collapse">

-  `Table Of Contents **** <#>`__

   -  `Welcome to Mouse\_TCR’s documentation! <#>`__
   -  `Indices and tables <#indices-and-tables>`__

`index <genindex.html>`__
`Show Source <_sources/index.rst.txt>`__

.. raw:: html

   <div class="input-append input-group">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="container-fluid">

.. raw:: html

   <div class="row">

.. raw:: html

   <div id="sidebar-wrapper" class="col-md-3 hidden-xs">

.. raw:: html

   <div class="sidebar hidden-xs" role="navigation"
   aria-label="main navigation">

.. rubric:: `Table Of Contents <#>`__
   :name: table-of-contents

-  `Welcome to Mouse\_TCR’s documentation! <#>`__
-  `Indices and tables <#indices-and-tables>`__

.. raw:: html

   <div role="note" aria-label="source link">

.. rubric:: This Page
   :name: this-page

-  `Show Source <_sources/index.rst.txt>`__

.. raw:: html

   </div>

.. raw:: html

   <div id="searchbox" role="search">

.. rubric:: Quick search
   :name: quick-search

.. raw:: html

   <div class="input-append input-group">

.. raw:: html

   </div>

Enter search terms or a module, class or function name.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="content-wrapper" class="col-md-9">

.. raw:: html

   <div class="document" role="main">

.. raw:: html

   <div class="documentwrapper">

.. raw:: html

   <div class="bodywrapper">

.. raw:: html

   <div class="body">

.. raw:: html

   <div id="welcome-to-mouse-tcr-s-documentation" class="section">

.. rubric:: Welcome to Mouse\_TCR’s
   documentation!\ `¶ <#welcome-to-mouse-tcr-s-documentation>`__
   :name: welcome-to-mouse_tcrs-documentation

.. raw:: html

   <div class="toctree-wrapper compound">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div id="indices-and-tables" class="section">

.. rubric:: Indices and tables\ `¶ <#indices-and-tables>`__
   :name: indices-and-tables

-  `Index <genindex.html>`__
-  `Module Index <py-modindex.html>`__
-  `Search Page <search.html>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="row footer-relbar">

.. raw:: html

   <div id="navbar-related" class="related navbar navbar-default"
   role="navigation" aria-label="related navigation">

.. raw:: html

   <div class="navbar-inner">

-  `Mouse\_TCR 1 documentation <#>`__

-  `index <genindex.html>`__
-  `top <#>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

© Copyright 2018, Adelaide Rhodes. Created using
`Sphinx <http://sphinx.pocoo.org/>`__ 1.7.2.

.. raw:: html

   </div>
