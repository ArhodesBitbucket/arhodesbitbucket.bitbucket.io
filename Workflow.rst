Notes on workflow
=================

Raw fastq files were provided without demultiplexing
----------------------------------------------------

.. raw:: html

   <div class="sourceCode">

.. code:: bash

       Boyson20180119_S1_L001_R1_001.fastq_.gz
       Boyson20180119_S1_L001_R2_001.fastq_.gz

.. raw:: html

   </div>

Files were "gunzipped"
----------------------

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        gunzip *.fastq

.. raw:: html

   </div>

Files were checked for quality using fastqc
-------------------------------------------

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        /gpfs1/mbsr_tools/Release2017-09-14/FastQC/fastqc *.fastq

.. raw:: html

   </div>

After checking the files in Fastqc, it was noted that the reverse reads,
"...R2\_001.fastq" were superior in quality. Reverse reads are usually
poorer quality. Also, read lengths were sometimes abnormally short, only
35 bp, which indicates that only the adapter was sequenced.

Here are the links to the output reports:
-----------------------------------------

`"Forward"
reads <https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/4c9b5b68b51748acdd4ac0991b8c1277c7641154/Boyson20180119_S1_L001_R1_001.fastq%20FastQC%20Report.pdf>`__
`"Reverse"
reads <https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/4c9b5b68b51748acdd4ac0991b8c1277c7641154/Boyson20180119_S1_L001_R2_001.fastq%20FastQC%20Report.pdf>`__

Based on poor quality near the end of the reads, the files were trimmed
and clipped.

Files were trimmed to remove sequences of base less than Q30 from the end, and minimum length of 100 after trimming
-------------------------------------------------------------------------------------------------------------------

FASTX Toolkit 0.0.13 by A. Gordon (gordon@cshl.edu) `FASTX Download
site <http://hannonlab.cshl.edu/fastx_toolkit/>`__

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        /gpfs1/mbsr_tools/Arhodes/FastX/bin/fastq_quality_trimmer -Q 33 -t 30 -l 100 -i Boyson20180119_S1_L001_R1_001.fastq -o Boyson20180119_S1_L001_R1_001.trimmed.fastq
        /gpfs1/mbsr_tools/Arhodes/FastX/bin/fastq_quality_trimmer -Q 33 -t 30 -l 100 -i Boyson20180119_S1_L001_R2_001.fastq -o Boyson20180119_S1_L001_R2_001.trimmed.fastq    

.. raw:: html

   </div>

Files were clipped to remove adapters, the trimming part was also redone starting with the Raw fastq files, using a sliding window approach this time, and sequences were grouped into paired and unpaired files. This is helpful for the next step, pandaseq. The rerun of the trimming was mainly due to switching to a different tool "Trimmomatic" for adapter removal, which can be read about here: `Trimmomatic website <http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf>`__
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        java -jar /gpfs1/mbsr_tools/Arhodes/Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads 12 Boyson20180119_S1_L001_R1_001.fastq Boyson20180119_S1_L001_R2_001.fastq forward_paired.fq forward_unpaired.fq reverse_paired.fq reverse_unpaired.fq ILLUMINACLIP:All.both.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:50

.. raw:: html

   </div>

Program output

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        Input Read Pairs: 14030383 
        Both Surviving: 13818842 (98.49%) 
        Forward Only Surviving: 195813 (1.40%) 
        Reverse Only Surviving: 8413 (0.06%) 
        Dropped: 7315 (0.05%)
        TrimmomaticPE: Completed successfully

.. raw:: html

   </div>

Files were rechecked for quality using fastqc
---------------------------------------------

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        /gpfs1/mbsr_tools/Release2017-09-14/FastQC/fastqc -t 2 *_paired.fq

.. raw:: html

   </div>

`"Forward"
reads <https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/0f1cc557dd712a2d73a560008bd19811c390753a/forward_paired.fq%20FastQC%20Report.pdf>`__
`"Reverse"
reads <https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/0f1cc557dd712a2d73a560008bd19811c390753a/reverse_paired.fq%20FastQC%20Report.pdf>`__

Pandaseq paired end assembler was used to combine the forward and reverse reads after trimming and clipping
-----------------------------------------------------------------------------------------------------------

`Paper to cite for
pandaseq <https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-13-31>`__

`Download instructions for
pandaseq <https://github.com/neufeld/pandaseq/wiki/Installation>`__

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        pandaseq -f forward_paired.fq -r reverse_paired.fq -T 12 -B -N -w combined.fa

.. raw:: html

   </div>

Number and length of reads checked after combining forward and reverse paired reads.
------------------------------------------------------------------------------------

.. raw:: html

   <div class="sourceCode">

.. code:: bash

    #Number of lines in forward paired reads

        grep "@M02" forward_paired.fq | wc -l
        13818842
        
    #Number of lines in reverse paired reads

        grep "@M02" reverse_paired.fq | wc -l
        13818842

    #Number of reads in combined file

        grep ">" combined.fa | wc -l
        13773226

    #Length distribution of combined reads

        awk '/^>/ {print; next; } { seqlen = length($0); print seqlen}' combined.fa | grep -v ">" | sort | uniq -c | sort -nk2
        

    #Read lengh between 50 and 500
    #Trimodal distribution at 67,282 and 350
    #In other words, the reads are all over the map, so pandaseq needs to be rerun with stricter parameters.

.. raw:: html

   </div>

Diagnosing the bimodal distribution of the combined.fa file
-----------------------------------------------------------

Two sequences were chosen at random from the combined.fa file

Sequence #1 \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        >M02840:176:000000000-BLLHM:1:1101:14865:1357:1
        CCATTGGGACGAGGCCAGGGTTTTCCCAGTCACGACGTCGATTTTCTGTGAAGCACAGCAAGGCCAACAGAACCTTCCATCTGGTGATCTCTCCAGTGAGCCTTGAAGACAGCGCTACTTATTACTGTGCCTCGGGGTATATCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAATGGAACAAATGTTGCTTGTCTGGTGTAAGTCA

.. raw:: html

   </div>

This fits into the shorter range (282)

When blasted, it hit Mouse TCR.

`See attached
picture <https://bytebucket.org/arhodes7/mouse_lung_vg4_tcr/raw/c63e5b6ae0b281485f342132cabdf68a3ea4ee6b/TCR_Blast.png?token=5a31099de9f4085a5ff64e2c7423bc0ee6dd7087>`__

Sequence #2 \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        >M02840:176:000000000-BLLHM:1:1101:18032:1644:1
        TTGCGGCAAAACTGCGTAACCGTCTTCTCGTTCTCTAAAAACCATTTTTCGTCCCCTTCTGGGCGGTGGTCTATAGTGTTATTAATATCAAGTTGGGGGAGCACATTGTAGCATTGTGCCAATTCATCCATTAACTTCTCAGTAACAGATACAAACTCATCACGAACGTCAGAAGCAGCCTTATGGCCGTCAACATACATATCACCATTATCGAACTCAACGCCCTGCATACGAAAAGACAGAATCTCTTCCAAGAGCTTGATGCGGTTATCCATCTGCTTATGGAAGCCAAGCATTGGGGATTGAGAAAGAGTAGAAATGCCACAAGCCTCAATAGCAGGTTTAAGAGCCTCGATACGCTCAAAGTCAAAATAATCAGCGTGACATTCAGAAGGGT

.. raw:: html

   </div>

This fits into the longer range (397)

When blasted, it hit PhiX.

`See attached
picture <https://bytebucket.org/arhodes7/mouse_lung_vg4_tcr/raw/496fc2400b41daf2382922f1a57f3087b920077e/PhiX_Blast.png?token=fcfb936a073d31c9afdff18d2a1a597706e492a6>`__

So, PhiX reads were not removed by the MiSeq sequencer.

PhiX reads were removed from trimmed reads
------------------------------------------

There is a program called
`SMALT <http://www.sanger.ac.uk/science/tools/smalt-0>`__, at Sanger
Sequencing that can remove reads for particular genomes. This was
recommended by a `google
group <https://groups.google.com/forum/#!topic/qiime-forum/yCQtm45tnDY>`__
for removing PhiX from MiSeq reads.

I downloaded the PhiX genome from
`NCBI <https://www.ncbi.nlm.nih.gov/nucleotide/NC_001422>`__.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        smalt index -k 14 -s 8 PhiX PhiX.fasta
        smalt map -n 12 -o mapped.sam PhiX forward_paired.fq reverse_paired.fq
        samtools view -S -f4 mapped.sam > unmapped.sam
        cat unmapped.sam | grep -v ^@ | awk 'NR%2==1 {print "@"$1"\n"$10"\n+\n"$11}' > unmapped.forward.fastq
        cat unmapped.sam | grep -v ^@ | awk 'NR%2==0 {print "@"$1"\n"$10"\n+\n"$11}' > unmapped.reverse.fastq

    #Check number of reads before combination
        grep "^@M02" unmapped.forward.fastq | wc -l
        11284829
        grep "^@M02" unmapped.reverse.fastq | wc -l
        11284828

.. raw:: html

   </div>

Pandaseq was rerun with a minimum length of 100. No maximum length was set.
---------------------------------------------------------------------------

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        pandaseq -l 100 -f forward_paired.fq -r reverse_paired.fq -T 12 -B -N -w combined.2ndattempt.fa

    #Check combined reads
        grep ">" combined.2ndattempt.fa | wc -l
        11186764

    #Check read distribution

        awk '/^>/ {print; next; } { seqlen = length($0); print seqlen}' combined.2ndattempt.fa | grep -v ">" | sort | uniq -c | sort -nk2

    #Read length between 100 and 500
    #Mean distribution peaks at 282, but covers a huge range.

.. raw:: html

   </div>

| So there are probably other contaminants in the data leading to a
  skewed distribution.
| This is where being told the anticpated insert size would be helpful,
  then reads could be pre-screened based on that.

Biological contamination
------------------------

Even though most of the PhiX was removed, fragments of PhiX and other
bacteria (environmental contamination) are still in our reads.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep -v CCAGGGTTTTCCCAGTCACGAC test.fa | head

.. raw:: html

   </div>

    M02840:176:000000000-BLLHM:1:1101:16508:1371:1
    TGTTATAGATATTCAAATAACCCTGAAACAAATGCTTAGGGATTTTATTGGTATCAGGGTTAATCGTGCCAAGAAAAGCGGCATGGTCAATATAACCAGTAGTGTTAACAGTCGGGAGAGGAGTGGCATTAACACCATCCTTCATGAACTTAATCCACTGTTCACCATAAACGTGACGATGAGGGACATAAAAAGTAAAAATGTCTACAGTAGAGTCAATAGCAAGGCCACGACGCAATGGAGAAAGACGGAGAGCGCCAACGGCGTCCATCTCGAAGGAGTCGCCAGCGATAACCGGAGTAGTTGAAATGGTAATAAGACGACC

Blasted to: Synthetic Enterobacteria phage CryptX174, complete genome
Sequence ID: MF426915.1Length: 5386Number of Matches: 1

1100 bp long, so that helps. Also, no primers.

However, this is another case where knowing the desired insert size
could save time.

Reads were size selected to remove other biological contaminants and
primer dimers.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

    Mode was used to find the most frequently observed length, which guides the minimum and maximum lengths of sequences to accept.  This also does not need to be rounded.

        mode=$(echo $(head -n 100000 $2 |  awk 'NR%2 == 0 {lengths[length($0)]++} END {for (l in lengths) {print l, lengths[l]}}' | sort -nk2 | tail -n1 | awk '{print $1}'))
        export mode
        lowerlimit=$(($mode -20))
        export lowerlimit
        upperlimit=$(($mode+20))
        export upperlimit


    #This function uses the calculated mode to size select sequences from the original fasta.
    #This function also converts multiline fasta into single line fasta for faster grepping.
    #Primer dimer was an issue, so a line was added to remove these before sorting.


            awk '/^>/ {printf("\n%s\t",$0);next; } { printf("%s\t",$0);}  END {printf("\n");}' $2 |\
            sed 's/ 1:N:0/:1:N:0/g' | sed '/^$/d' |\
            awk '!/AGGGTTTTCCCAGTCACGA[CGTAN]{0,8}TTTTCTGTGAAGCACAGCA/' |\
            awk -v ll="$lowerlimit" -v ul="$upperlimit" 'length($2)>ll && length($2)<ul' >$2.size.selected

.. raw:: html

   </div>

The mode was 282, so the size selection was between 262 and 302. I am
including a picture of the histogram for your reference.

`See picture for distribution before size
selection. <https://bytebucket.org/arhodes7/mouse_lung_vg4_tcr/raw/f245e3ca511f770c103ac8b2f65d434e1876d6a7/histogram.boyson.jpg?token=312c9d85c3ea9208681d0dd490fa289dd42a4cd6>`__

Many of the issues actually had to do with primers.

Problems with Primers
---------------------

Primer-dimer or the 3rd reaction primer brings the 2nd reaction primer
into the read.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep GCAGAGATAAGC combined.2ndattempt.fa | grep TGAAC | head | grep TGAAC
        
        grep AGATAAGGAGTACAAGAAAATGGA ../mouse_lung_vg4_tcr/Primer_Set.txt

.. raw:: html

   </div>

2mouseGV4 CCAGGGTTTTCCCAGTCACGACNNCTTAGATAAGGAGTACAAGAAAATGGA

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep M02840:176:000000000-BLLHM:1:1101:16458:2829:1 -A1 ../combined.2ndattempt.fa | grep AGATAAGGAGTACAAGAAAATGGA --color

.. raw:: html

   </div>

AGATCCTGATGTTG\ **CCAGGGTTTTCCCAGTCACGAC**\ GGCTTAGATAAGGAGTACAAGAAAATGGAGGCAAGTAAAAATCCTAGTGCTTCTACATCGATATTGACAATATATTCCTTGGAGGAAGAAGACGAAGCTATCTACTACTGTTCCTGCGGCTTATATAGCCCAGGTTTTCACAAGGTATTTGCAGAAGGAACTAAGCTCATAGTAATTCCCTCTGACAAAAGGCTTGATGCAGACATTTCCCCCAAGCCCACTATTTTCCTTCCTTCTGTTGCTGAAACAAATCACGTCGA

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep M02840:176:000000000-BLLHM:1:1101:16458:2829:1 -A1 P9G8_Vg4_Mouse_9_Sort_4_M_FBAR_ATCCTGATGTTG_RBAR_GACGT.fasta | grep CTTAGATAAGGAGTACAAGAAAATGGA --color

.. raw:: html

   </div>

TTAGATAAGGAGTACAAGAAAATGGAGGCAAGTAAAAATCCTAGTG\ **CTTCTACATCGATATTGACAATATATTCCTTGGA**\ GGAAGAAGACGAAGCTATCTACTACTGTTCCTGCGGCTTATATAGCCCAGGTTTTCACAAGGTATTTGCAGAAGGAACTAAGCTCATAGTAATTCCCTCTGACAAAAGGCTTGATGCAGACATTTCCCCCAAGCCCACTATTTTCCTTCCTTCTGTTGCTGAAACAAATC

This is a problem, because we cannot strip the primers using a single
position in the read.

This sequence is longer, maybe that could be used to detect? But that
may get rid of true inserts. Possibly screening resulting fasta files by
primer? Or just using mode to remove sequences within a smaller range,
say 10 bp plus or minus instead of 20?

Or just reject longest sequences before alignment?

Problem: We may be rejecting true biological information.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

    egrep -e "CCAGGGTTTTCCCAGTCACGACG[CGTAN]{0,2}GATTTTCTGTGAAGCACAGCAAG" ../combined.2ndattempt.fa | head | egrep -e "CCAGGGTTTTCCCAGTCACGACG[CGTAN]{0,2}GATTTTCTGTGAAGCACAGCAAG"

    egrep -e "CCAGGGTTTTCCCAGTCACGACG[CGTAN]{0,2}GATTTTCTGTGAAGCACAGCAAG" ../combined.2ndattempt.fa | wc -l

    #1097463

    wc -l ../combined.2ndattempt.fa

    #22373528 ../combined.2ndattempt.fa

.. raw:: html

   </div>

This would remove about 5% of the reads. Not a big loss.

Primer Degeneracy
-----------------

The primers changed sequence enough that not one sequence could be used
to search and remove them. Also, upon searching, not enough uniq
sequence was found in long enough stretches to allow for a direct search
using primers.

| CC A GGGT T TTCCC A G T C A CG A C
| CC A GGGT T TTCCC G G T C A CG A C
| CC A GGGT C TTCCC A G T C A CG A C
| CC G GGGT T TTCCC A G T C A CG A C
| CC A GGGT T TTCCC G G T C A CG A C
| CC G GGGT TTCC C G G T C A CG A C
| CC A GGGT T TTCCC A G T A A CG A C
| CC G GGGT TTCC C G G T C A CG A C
| CC A GGGT TTCC C A G T C A CG A C
| CC A GGGT T TTCCC A C T C A CG A C
| CC A GGGT T TTCCC A G T C A CG C C
| CC A GGGT T TTCCC A G T C A G A C
| CC A GGGT T TTCCC A G T C G CG A C

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        awk '/CC[CGAT]{1}GGGT[CGAT]{0,1}TTCCC[CGTA]{2}T[CGTA]{1}A[C]{0,1}[GCTA]{1,2}C/' test.fa.size.selected | wc -l
    #5971
        awk '/CC[CGAT]{1}GGGT[CGAT]{1}TTCCC[CGTA]{1}GTCACGAC/' test.fa.size.selected | wc -l
    #5897
        awk '/CCAGGGTTTTCCCAGTCACGAC/' test.fa.size.selected | wc -l
    #5860
        awk '!/CCAGGGTTTTCCCAGTCACGAC/' test.fa.size.selected | wc -l
    #463

.. raw:: html

   </div>

About 8% of the data would be removed if only exact primers were used
for the search.

Primers are not at the end of the sequence
------------------------------------------

In 99% of the projects, sequence comes off the sequencer already
demultiplexed and with the primers on the end of the reads.

This method does not do that. It has primers inside the barcodes.

If the primers are left in, it is not possible to cluster the data
efficiently.

The ideal sequence in this case would come off the sequencer like this:

barcode1 primer1 insert primer2 barcode2

If all of the primers were the same size then we could just subtract
them out based on position.

However, this project has several primer sizes, so it is not possible to
choose one position for stripping them.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

    head primer_hits.txt

.. raw:: html

   </div>

| M02840:176:000000000-BLLHM:1:1101:15793:1372:1 15 66 +
| M02840:176:000000000-BLLHM:1:1101:16219:1372:1 15 62 +
| M02840:176:000000000-BLLHM:1:1101:15167:1374:1 15 62 +
| M02840:176:000000000-BLLHM:1:1101:14622:1378:1 15 62 +
| M02840:176:000000000-BLLHM:1:1101:16367:1376:1 15 65 +
| M02840:176:000000000-BLLHM:1:1101:15440:1378:1 15 62 +
| M02840:176:000000000-BLLHM:1:1101:15052:1368:1 15 62 +
| M02840:176:000000000-BLLHM:1:1101:16810:1378:1 15 65 +
| M02840:176:000000000-BLLHM:1:1101:16029:1380:1 15 62 +
| M02840:176:000000000-BLLHM:1:1101:14994:1381:1 15 65 +

.. raw:: html

   <div class="sourceCode">

.. code:: bash

    grep -v 62 primer_hits.txt | head

.. raw:: html

   </div>

| M02840:176:000000000-BLLHM:1:1101:15793:1372:1 15 66 +
| M02840:176:000000000-BLLHM:1:1101:16367:1376:1 15 65 +
| M02840:176:000000000-BLLHM:1:1101:16810:1378:1 15 65 +
| M02840:176:000000000-BLLHM:1:1101:14994:1381:1 15 65 +
| M02840:176:000000000-BLLHM:1:1101:14920:1366:1 15 65 +
| M02840:176:000000000-BLLHM:1:1101:16736:1412:1 15 65 +
| M02840:176:000000000-BLLHM:1:1101:15090:1365:1 15 65 +
| M02840:176:000000000-BLLHM:1:1101:17112:1437:1 15 65 +
| M02840:176:000000000-BLLHM:1:1101:16701:1381:1 13 60 +
| M02840:176:000000000-BLLHM:1:1101:15540:1384:1 15 63 +

.. raw:: html

   <div class="sourceCode">

.. code:: bash

    grep -v 15 primer_hits.txt | wc -l
    #1796203

.. raw:: html

   </div>

The different sized primers were designed to find different types of
sequence, so using size selection would eliminate that diversity.

Using the 15,62 settings would remove 21% of the sequences that had
legitimate primers, and which contained diverse biological data - the
kind you are looking for in this project.

| 2mouseDV1 CCAGGGTTTTCCCAGTCACGACNNCGCTAAGCTGGATAAGAAAATGCAG
| 2mouseDV2 CCAGGGTTTTCCCAGTCACGACNNCTCTGTGAACTTCCAGAAAGCAGC
| 2mouseDV4 CCAGGGTTTTCCCAGTCACGACNNCCTCAAAGGGAAAATTAACATTTCAAA
| 2mouseDV5 CCAGGGTTTTCCCAGTCACGACNNCGATTTTCTGTGAAGCACAGCAAG
| 2mouseDV6 CCAGGGTTTTCCCAGTCACGACNNCTAYTCTGTAGTCTTCCAGAAATCA
| 2mouseDV7 CCAGGGTTTTCCCAGTCACGACNNGTMCAATCCTTCTGGGACAAAGCA
| 2mouseDV8 CCAGGGTTTTCCCAGTCACGACNNGATTCACAATCTTCTTCAATAAAAGGGA
| 2mouseDV9 CCAGGGTTTTCCCAGTCACGACNNGGAAGCAGCAGAGGKTTTGAAGC
| 2mouseDV10 CCAGGGTTTTCCCAGTCACGACNNGGGAGGCTAAAGTCAGCATTTGAT
| 2mouseDV11 CCAGGGTTTTCCCAGTCACGACNNGGTCATTATTCTCTGAACTTTCAGAAG
| 2mouseDV12 CCAGGGTTTTCCCAGTCACGACNNGGCTATTGCCTCTGACAGAAAGT
| 2mouseGV1-3 CCAGGGTTTTCCCAGTCACGACNNACAARAAAATTGAAGCAAGTAAAGATTTT
| 2mouseGV4 CCAGGGTTTTCCCAGTCACGACNNCTTAGATAAGGAGTACAAGAAAATGGA
| 2mouseGV5 CCAGGGTTTTCCCAGTCACGACNNCCACTCCCGCTTGGAAATTGATGA
| 2mouseGV6 CCAGGGTTTTCCCAGTCACGACNNGTGACGAAAGATATGAGGCAAGGA
| 2mouseGV7 CCAGGGTTTTCCCAGTCACGACNNCATGTTTATGAAGGCCCGGACAAGA

.. raw:: html

   <div class="sourceCode">

.. code:: bash

     grep "\sCCAGGGTTTTCCCAGTCACGAC" Primer_Set.txt | awk '{print $2}' | while read line; do echo $line| wc -c ; done

.. raw:: html

   </div>

| 50
| 49
| 52
| 49
| 50
| 49
| 53
| 48
| 49
| 52
| 48
| 54
| 52
| 49
| 49
| 50

Primer Problems Require Computer intensive code
-----------------------------------------------

JOBS CURRENTLY RUNNING.

822 JOBS X 1.5 HOURS EACH /20 MAX RUNNING AT ONE TIME = 61 HOURS TO
STRIP PRIMERS, AND WE HAVEN'T EVEN GOTTEN TO DEMULTIPLEXING YET

| 17182150.moab-mgmt1.cluster SPLITPS2 arhodes 00:31:38 R bsrq
| 17182148.moab-mgmt1.cluster SPLITPS2 arhodes 00:32:00 R bsrq
| 17182149.moab-mgmt1.cluster SPLITPS2 arhodes 00:31:44 R bsrq
| 17182230.moab-mgmt1.cluster SPLITPS2 arhodes 00:36:43 R bsrq
| 17187129.moab-mgmt1.cluster SPLITPS8 arhodes 02:06:34 C poolmemq
| 17187130.moab-mgmt1.cluster SPLITPS8 arhodes 02:06:36 C poolmemq
| 17187131.moab-mgmt1.cluster SPLITPS8 arhodes 01:52:36 R poolmemq
| 17187132.moab-mgmt1.cluster SPLITPS8 arhodes 00:57:11 R poolmemq
| 17187133.moab-mgmt1.cluster SPLITPS8 arhodes 00:57:01 R poolmemq
| 17187134.moab-mgmt1.cluster SPLITPS8 arhodes 00:56:58 R poolmemq
| 17187135.moab-mgmt1.cluster SPLITPS8 arhodes 00:56:35 R poolmemq
| 17187136.moab-mgmt1.cluster SPLITPS8 arhodes 00:56:30 R poolmemq
| 17187137.moab-mgmt1.cluster SPLITPS8 arhodes 00:55:55 R poolmemq
| 17187138.moab-mgmt1.cluster SPLITPS8 arhodes 00:46:21 R poolmemq
| 17187139.moab-mgmt1.cluster SPLITPS8 arhodes 00:46:23 R poolmemq
| 17187140.moab-mgmt1.cluster SPLITPS8 arhodes 00:46:42 R poolmemq
| 17187141.moab-mgmt1.cluster SPLITPS8 arhodes 00:45:31 R poolmemq
| 17187142.moab-mgmt1.cluster SPLITPS8 arhodes 00:42:05 R poolmemq

New Approach Feb, 2018
----------------------

After consulting with the researcher, it seemed that removing primers
would be simpler by stripping the primers during demultiplexing.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        #!/bin/bash
        cat $1|\
        #echo "$1" |\
        while read sampleid f_barcode r_barcode sortorder mouse gender celltype prime3 cell3prime  prime5 cell5prime

        ##while read sampleid f_barcode r_barcode sortorder mouse gender celltype prime3 cell3prime  prime5delta cell5primedelta

        do

        #echo $prime3

        #This first line creates a sample ID that will be used as the new file name.  Each file will be named $SampleID.fastq.  Each barcode combination will create a fastq.

        #P1A1   GCAGAGATAAGC    GTTCA   1   1   M   Vg4     GTTCACACCAGACAAGCAACATTTGTTCC   DCbc1   AACAAGAAGGAGCCAGGGTTTTCCCAGTCACGAC  V3F

        SampleID=$(echo $sampleid"_"$celltype"_Mouse_"$mouse"_Sort_"$sortorder"_"$gender"_FBAR_"$f_barcode"_RBAR_"$r_barcode"_5PRIME_"$cell5prime"_3PRIME_"$cell3prime)

        export SampleID
        echo $SampleID

        echo "This set of reads will be saved in " $SampleID.fasta

        # 
        # Make reverse complements of our reverse barcodes
        # 
        rc_r_barcode=$(echo $prime3|rev|tr ACGT[] TGCA][)
        export rc_r_barcode
        echo $rc_r_barcode
     
        lenrcr=$(echo $rc_r_barcode| awk '{print length($1)}')
        export lenrcr
        echo $lenrcr
            
        f_barcode_fn=$(echo $prime5)
        export f_barcode_fn
        echo $f_barcode_fn
     
        searchstring=$(echo "[[:space:]][CGATN]{2}$f_barcode_fn.*$rc_r_barcode[CGATN]{2}[[:space:]]")   
            export searchstring 
            echo $searchstring
        # 
        #       

        LC_ALL=C egrep -e "$searchstring" $2 | awk -v lenr="$lenrcr" '{begin=39}{end=length($2)-40-lenr}{print $1"\n" substr($2,begin,end)}' >> Demultiplexed.trimmed/$SampleID.fasta

        done

.. raw:: html

   </div>

This task was split into two sets, the sequences with V-delta chains and
those without.

There were 69120 possible combinations of primers and well ID's, so the
search algorithm was split into 6 sets for the primers with delta
chains.

The original sample sheets are in the source files.

`Link to sample sheet to search reads with delta chains (2689512
reads). <https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/e47f3a53341b642c5d7e4be5d0002d4280ad0cd0/Sample_Sheet.p3.p5.txt>`__

`Link to the sample sheet without the delta chains (262258
reads). <https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/e47f3a53341b642c5d7e4be5d0002d4280ad0cd0/Sample_Sheet.p3.p5.delta.txt>`__

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        sh demultiplex.only.nodelta.sh Sample_Sheet.delta.10000 combined.2nd.fa.ss.wtdelta
        sh demultiplex.only.nodelta.sh Sample_Sheet.delta.20000 combined.2nd.fa.ss.wtdelta
        sh demultiplex.only.nodelta.sh Sample_Sheet.delta.30000 combined.2nd.fa.ss.wtdelta
        sh demultiplex.only.nodelta.sh Sample_Sheet.delta.40000 combined.2nd.fa.ss.wtdelta
        sh demultiplex.only.nodelta.sh Sample_Sheet.delta.50000 combined.2nd.fa.ss.wtdelta
        sh demultiplex.only.nodelta.sh Sample_Sheet.delta.60000 combined.2nd.fa.ss.wtdelta
        sh demultiplex.only.nodelta.sh Sample_Sheet.delta.69120 combined.2nd.fa.ss.wtdelta
        sh demultiplex.only.nodelta.sh Sample_Sheet.p3.p5.txt combined.2nd.fa.ss.nodelta

.. raw:: html

   </div>

This is how the sequence with and without delta chains were separated.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        awk '!/CGCTAAGCTGGATAAGAAAATGCAG/ && !/CTCTGTGAACTTCCAGAAAGCAGC/ && !/CCTCAAAGGGAAAATTAACATTTCAAA/ && !/CGATTTTCTGTGAAGCACAGCAA/ && !/CTA[ACGT]TCTGTAGTCTTCCAGAAATCA/ && !/GT[ACGT]CAATCCTTCTGGGACAAAGCA/ && !/GATTCACAATCTTCTTCAATAAAAGGGA/ && !/GGAAGCAGCAGAGG[ACGT]TTTGAAGC/ && !/GGGAGGCTAAAGTCAGCATTTGAT/ && !/GGTCATTATTCTCTGAACTTTCAGAAG/ && !/GGCTATTGCCTCTGACAGAAAGT/ && !/ACAA[ACGT]AAAATTGAAGCAAGTAAAGATTTT/ && !/CTTAGATAAGGAGTACAAGAAAATGGA/ && !/CCACTCCCGCTTGGAAATTGATGA/ && !/GTGACGAAAGATATGAGGCAAGGA/ && !/CATGTTTATGAAGGCCCGGACAAGA/' combined.2ndattempt.fa.size.selected  > combined.2nd.fa.ss.nodelta

        awk '/CGCTAAGCTGGATAAGAAAATGCAG/ || /CTCTGTGAACTTCCAGAAAGCAGC/ || /CCTCAAAGGGAAAATTAACATTTCAAA/ || /CGATTTTCTGTGAAGCACAGCAA/ || /CTA[ACGT]TCTGTAGTCTTCCAGAAATCA/ || /GT[ACGT]CAATCCTTCTGGGACAAAGCA/ || /GATTCACAATCTTCTTCAATAAAAGGGA/ || /GGAAGCAGCAGAGG[ACGT]TTTGAAGC/ || /GGGAGGCTAAAGTCAGCATTTGAT/ || /GGTCATTATTCTCTGAACTTTCAGAAG/ || /GGCTATTGCCTCTGACAGAAAGT/ || /ACAA[ACGT]AAAATTGAAGCAAGTAAAGATTTT/ || /CTTAGATAAGGAGTACAAGAAAATGGA/ || /CCACTCCCGCTTGGAAATTGATGA/ || /GTGACGAAAGATATGAGGCAAGGA/ || /CATGTTTATGAAGGCCCGGACAAGA/' combined.2ndattempt.fa.size.selected  > combined.2nd.fa.ss.wtdelta

.. raw:: html

   </div>

After demultiplexing, 2400010 total reads were assigned to 15789 files
(69120 of the combined primer/well possibilities)

1412 wells had between 1 and 17 primer combinations.

Cell sort order had different distributions of unique primer
combinations:

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        ls Demultiplexed.trimmed/*fasta | cut -d "_" -f6 | sort | uniq -c | sort -nk2 | awk '{print "\t"$2"\t"$1}'

        #   1   1367
        #   2   1987
        #   3   4441
        #   4   4614
        #   5   3380
     

.. raw:: html

   </div>

Mice had different distrubtions of unique primer combinations:

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        ls Demultiplexed.trimmed/*fasta | cut -d "_" -f4 | sort | uniq -c | sort -nk2 | awk '{print "\t"$2"\t"$1}'

        #   1   573    
        #   2   794   
        #   3   984
        #   4   1003  
        #   5   987
        #   6   1140
        #   7   1131
        #   8   1183
        #   9   1076
        #   10  1150
        #   11  1226
        #   12  1162
        #   13  1188
        #   14  979
        #   15  1213

.. raw:: html

   </div>

To facilitate assignment to IMGT designations, the raw read duplicates
were collapsed within each well/primer group.

For example, well P10A10 may have had 13 combinations of primers, with
3804 total reads.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        wc -l Demultiplexed.trimmed/P10A10*

        #     4 Demultiplexed.trimmed/P10A10_Vg4_Mouse_10_Sort_4_M_FBAR_ATGTCGATAAGC_RBAR_GAATG_5PRIME_V10A_2DV12_3PRIME_DCbc10.fasta
        #    14 Demultiplexed.trimmed/P10A10_Vg4_Mouse_10_Sort_4_M_FBAR_ATGTCGATAAGC_RBAR_GAATG_5PRIME_V10A_2DV2_3PRIME_DCbc10.fasta
        #  2316 Demultiplexed.trimmed/P10A10_Vg4_Mouse_10_Sort_4_M_FBAR_ATGTCGATAAGC_RBAR_GAATG_5PRIME_V10A_2DV5_3PRIME_DCbc10.fasta
        #   468 Demultiplexed.trimmed/P10A10_Vg4_Mouse_10_Sort_4_M_FBAR_ATGTCGATAAGC_RBAR_GAATG_5PRIME_V10A_2DV6_3PRIME_DCbc10.fasta
        #   104 Demultiplexed.trimmed/P10A10_Vg4_Mouse_10_Sort_4_M_FBAR_ATGTCGATAAGC_RBAR_GAATG_5PRIME_V10A_2DV8_3PRIME_DCbc10.fasta

.. raw:: html

   </div>

But a lot of these reads are identical. NOTE, number of reads is 1/2 of
the line count because these are fasta files -- 1 header line followed
by one seuqence line.

To faciliate identificatoin of all the cell types that were captured by
the primers, the duplicate reads were collapsed and counted.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        ls Demultiplexed.trimmed/*| while read filename; do fastx_collapser -i $filename -o - |  awk -v "FILE=$filename" '/^>/ {gsub(/.fa(sta)?$/,"",FILE);printf("%s-%s\n",$0, FILE);next;} {print}' | awk -F "-" '/>/{print ">"$2"-"$3"_"$4;next}{print}' ;done | sed 's/_$//g' > corrected_collapsed.fasta

.. raw:: html

   </div>

Once collapsed, the 7 reads that were found in
P10A10\_Vg4\_Mouse\_10\_Sort\_4\_M\_FBAR\_ATGTCGATAAGC\_RBAR\_GAATG\_5PRIME\_V10A\_2DV2\_3PRIME\_DCbc10.fasta
are now collapsed to five unique reads. The counts are at the beginning
of the line.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep  -A1 P10A10_Vg4_Mouse_10_Sort_4_M_FBAR_ATGTCGATAAGC_RBAR_GAATG_5PRIME_V10A_2DV2_3PRIME_DCbc10 corrected_collapsed.fasta

.. raw:: html

   </div>

    2-Demultiplexed.trimmed/P10A10\_Vg4\_Mouse\_10\_Sort\_4\_M\_FBAR\_ATGTCGATAAGC\_RBAR\_GAATG\_5PRIME\_V10A\_2DV2\_3PRIME\_DCbc10
    CTCTGTGAACTTCCAGAAAGCAGCTAAGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATTTCTGTGCTCTCATGGAGCGCGGTCGGAGGGATACGAGCAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT
    2-Demultiplexed.trimmed/P10A10\_Vg4\_Mouse\_10\_Sort\_4\_M\_FBAR\_ATGTCGATAAGC\_RBAR\_GAATG\_5PRIME\_V10A\_2DV2\_3PRIME\_DCbc10
    CTCTGTGAACTTCCAGAAAGCAGCTAAGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATTTCTGTGCTCTCATGGAGCTCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT
    1-Demultiplexed.trimmed/P10A10\_Vg4\_Mouse\_10\_Sort\_4\_M\_FBAR\_ATGTCGATAAGC\_RBAR\_GAATG\_5PRIME\_V10A\_2DV2\_3PRIME\_DCbc10
    CTCTGTGAACTTCCAGAAAGCAGCTAAGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATTTCTGTGCTCTCATGGAGCGCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT
    1-Demultiplexed.trimmed/P10A10\_Vg4\_Mouse\_10\_Sort\_4\_M\_FBAR\_ATGTCGATAAGC\_RBAR\_GAATG\_5PRIME\_V10A\_2DV2\_3PRIME\_DCbc10
    CTCTGTGAACTTCCAGAAAGCAGCTAAGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATTTCTGTGCTCTCATGGAGCGCGGGGGATACGAGACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTCTCATCATGAAAAAT
    1-Demultiplexed.trimmed/P10A10\_Vg4\_Mouse\_10\_Sort\_4\_M\_FBAR\_ATGTCGATAAGC\_RBAR\_GAATG\_5PRIME\_V10A\_2DV2\_3PRIME\_DCbc10
    CTCTGTGAACTTCCAGAAAGCAGCTAGGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATTTCTGTGCTCTCATGGAGATCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        #Headers were rearranged to put the duplicate counts at the end of the line and remove the "Demultiplexed.trimmed."

        awk -F "-" '/>/{print ">"$2"-"$1 ;next}{print}' corrected.collapsed.fasta | sed 's/->/-/g' | sed 's/Demultiplexed.trimmed\///g > final.demultiplexed.fasta

        head final.demultiplexed.fasta

.. raw:: html

   </div>

    P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV12\_3P\_DCbc10-1
    GGCTATTGCCTCTGACAGAAAGTCAAGCACCTTGATCCTGCCTCATGTCAGCCTGAGAGACGCGGCTGTGTACCACTGTATCCTGAGATATATGGCCCCTTATCGGAGGGATACGAAAACCGACAAACTCGTCTTTGGACAGGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT
    P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV12\_3P\_DCbc10-1
    GGCTATTGCCTCTGACAGAAAGTCAAGCACCTTGATCCTGCCTCATGTCAGCCTGAGAGACGCGGCTGTGTACCACTGTATCCTGAGAGGAATATATCGGAGGGATACCGAATTTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT
    P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV2\_3P\_DCbc10-2
    CTCTGTGAACTTCCAGAAAGCAGCTAAGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATTTCTGTGCTCTCATGGAGCGCGGTCGGAGGGATACGAGCAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT
    P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV2\_3P\_DCbc10-2
    CTCTGTGAACTTCCAGAAAGCAGCTAAGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATTTCTGTGCTCTCATGGAGCTCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT
    P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV2\_3P\_DCbc10-1
    CTCTGTGAACTTCCAGAAAGCAGCTAAGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATTTCTGTGCTCTCATGGAGCGCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT

Each one of these reads could be a different cell type. This step just
identified the reads. The primers have been stripped.

Before combining duplicates: 2400010 total reads in 15789 files (69120
possibilities)

After combining duplicates WITHIN files: 981384 unique reads in 15789
files (2280177 total reads)

Reads from all of the files were combined to create an OTU table that
could describe the entire set of unique reads.

This is based on a common method in metagenomics, the usearch algorithm
`described at this
link. <https://drive5.com/usearch/manual/uclust_algo.html>`__

.. raw:: html

   <div class="sourceCode">

.. code:: bash

     
        usearch -otutab collapsed.reheaded.fasta -otus otus.fa -otutabout otutab.final.txt -mapout map.final.txt -uparseout demult.final
     

.. raw:: html

   </div>

When reads were collapsed in the last step, that was done within files,
so that wells could be treated uniquely if we want to go back and look
at individual sequences captured by primer sets.

To make an otu table for the entire data set, all reads from all files
are collapsed into unique reads across the entire experiment.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        usearch -fastx_uniques final.demultiplexed.fasta -fastaout uniques.fasta -sizeout -relabel Uniq
        head uniques.fasta

.. raw:: html

   </div>

    Uniq1;size=123773;
    CGATTTTCTGTGAAGCACAGCAAGGCCAACAGAACCTTCCATCTGGTGATCTCTCCAGTGAGCCTTGAAGACAGCGCTAC
    TTATTACTGTGCCTCGGGGTATATCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTG
    TGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT
    Uniq2;size=72963;
    CTCTGTGAACTTCCAGAAAGCAGCTAAGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATT
    TCTGTGCTCTCATGGAGCGCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAA
    CCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT

Size refers to how many reads from final.demultiplexed.fasta are part of
that set.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        awk -F "=" '/>/{print $2}' uniques.fasta | sed 's/;//g' | awk '{sum+=$1}END{print sum}'

        #2400010 -- same as the number of reads entered into the analysis.

        grep ">" uniques.fasta | wc -l

        #479112 --- same as the number of unique reads AMONG samples.

        #This option was not set, because unique reads may actually be biological: -sizeout -minuniquesize 2 

.. raw:: html

   </div>

Unique sequences that were singletons were not removed, because these
could be legitimate biologial sequences, and not sequencing error.

The next step clusters these unique reads by 97% similarity. This allows
for potential sequencing error.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        usearch -cluster_otus uniques.fasta -otus otus.fa -uparseout uparse.txt -relabel Otu -notmatched unmatchedotus.fasta

        head otus.fa

.. raw:: html

   </div>

    Otu1
    CGATTTTCTGTGAAGCACAGCAAGGCCAACAGAACCTTCCATCTGGTGATCTCTCCAGTGAGCCTTGAAGACAGCGCTAC
    TTATTACTGTGCCTCGGGGTATATCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTG
    TGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT Otu2
    CTCTGTGAACTTCCAGAAAGCAGCTAAGTCCTTCAGCCTGGAGATCTCCGACTCGCAGCTGGGGGATGCTGCGACGTATT
    TCTGTGCTCTCATGGAGCGCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAA
    CCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAAT

Because the data set is so diverse, a few more iterations were done.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep ">" unmatchedotus.fasta | wc -l
        #110361

        usearch -fastx_uniques unmatchedotus.fasta -fastaout uniques.unmatched.fasta -sizeout -relabel Uniq

        usearch -cluster_otus uniques.unmatched.fasta -otus otus.unmatched.fa -uparseout uparse.unmatched.txt -relabel OtuUnm -notmatched unmatched.2nd.fasta

        grep ">" unmatched.2nd.fasta | wc -l
        #31939

        usearch -fastx_uniques unmatched.2nd.fasta -fastaout uniques.unmatched.2nd.fasta -sizeout -relabel Uniq

        usearch -cluster_otus uniques.unmatched.fasta -otus otus.unmatched.2nd.fa -uparseout uparse.unmatched.2nd.txt -relabel OtuUnmat -notmatched unmatched.3rd.fasta

        grep ">" unmatched.3rd.fasta | wc -l
        #17011

        wc -l uniques*

.. raw:: html

   </div>

This approach had diminishing returns.

1916448 uniques.fasta 310476 uniques.unmatched.fasta 113288
uniques.unmatched.2nd.fasta 65624 uniques.unmatched.3rd.fasta 50020
uniques.unmatched.4th.fasta 43252 uniques.unmatched.5th.fasta 40564
uniques.unmatched.6th.fasta

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep ">" unmatched.4th.fasta | wc -l
        #12657

        grep ">" unmatched.5th.fasta | wc -l
        #10868

        grep ">" unmatched.6th.fasta | wc -l
        #10154

.. raw:: html

   </div>

The clustering seemed to be less effective after the third round, so a
centroid approach was used to lower the percentage similarity to capture
as much diversity as possible in the 65624 sequences left after
clustering for the third time.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        usearch -cluster_fast unmatched.3rd.fasta -id 0.9 -centroids otus90.fa -uc unmatched.90.fasta

        grep ">" otus90.fa | wc -l
        #254

        grep ">" unmatched.90.fasta | wc -l
        #15947

.. raw:: html

   </div>

It doesn't make sense to go much lower than 90% identity for these new
clusters.

I did send the unclustered reads to IMGT for analysis, but it doesn't
make much sense to look at those yet, because later in the analysis, we
rerun all reads against all OTU's, and that leftover set will most
likely contain these unclustered reads.

Chimera filtering
-----------------

No chimera filtering was done on the raw clusters.

| Usually, when clustering is used to define groups of reads, chimera
  filtering is turned on.
| Because these reads have legitimate combinations of V,G,D chains, the
  chimera filter would not be appropriate. It might read a new
  combination as a chimera of two other transcripts and reject it.

The otus.fa, otus.unmatched.fa, otus.unmatched.2nd.fa,
otus.unmatched.3rd.fa and otus90.fa were all combined to create 833
potential OTU's for capturing the diversity of the sequences.

A read can be in an OTU with another read, and not be 100% identical,
but they are close enough based on 97% similarity after the next mapping
step.

Even though some of the clusters were constructed using a slightly lower
identity, the final mapping will only place reads in the more ambiguous
clusters if a highly similar cluster is not found first.

Very few reads are in those last 254 OTU's. (6866 of the 981384 unique
reads from within fasta files). But these could be real biologically,
just very rare.

The OTU file was given new headers to help organize the information.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        cat otus.fa otus.unmatched.fa otus.unmatched.2nd.fa otus.unmatched.3rd.fa otus90.fa > combined.1.2.3.90.fasta

        awk '/^>/{print ">OTU_" n++; next}{print}' > combined.1.2.3.90.fasta > combined.1.2.3.90.rehead.fasta

        #Note:  I should have done this to start at OTU_1 instead of OTU_0: awk '{if (/^>/) print ">OTU_"(++i); else print $0;}'
        #or this awk '/>/{print ">OTU_"(++i); next}{print $0;}'

.. raw:: html

   </div>

These OTU's were sent to the IMGT for analysis, those files are too big
to put in the bitbucket, but were provided by FTP to the researcher.

The final OTU file had 833 OTU's in it.

Now, unique reads from each fasta file were mapped to the OTUS.

A few possibility exists for each read.

Two reads that were considered unique by the fastx\_collapser are
recombined into one OTU.

Well1\_PrimerCombination1\_UniqueRead1 --> Maps to OTU\_53
Well1\_PrimerCombination1\_UniqueRead2 --> Maps to OTU\_53

Two reads that were considered unique by the fastx\_collapser are
assigned to different OTU's.

Well1\_PrimerCombination1\_UniqueRead1 --> Maps to OTU\_53
Well1\_PrimerCombination1\_UniqueRead2 --> Maps to OTU\_57

Or, one or both of the reads does not find a match and ends up in the
unmapped file.

The highest number of potential OTU's that can be captured is equal to
the highest number of UNIQUE reads in a fasta file.

Each fasta file represents a unique combination of well location and
primer set.

It would not be expected to see a read from a different primer set map
into the same OTU as a read from another primer set in the same well.

This would be unusual:

Well1\_PrimerCombination1\_UniqueRead1 --> Maps to OTU\_53
Well1\_PrimerCombination2\_UniqueRead2 --> Maps to OTU\_53

This is to be expected:

Well1\_PrimerCombination1\_UniqueRead1 --> Maps to OTU\_53
Well1\_PrimerCombination2\_UniqueRead2 --> Maps to OTU\_99

Some primer combinations were very common:

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep ">" final.demultiplexed.fasta | cut -d "-" -f1 | cut -d "_" -f 8- | sort | uniq -c | sort -nk1 | tail

.. raw:: html

   </div>

Ten most common primer combinations

::

       1090 5P_V13F_2DV2_3P_DCbc5
       1148 5P_V12C_2DV2_3P_DCbc7
       1229 5P_V15C_2DV2_3P_DCbc4
       1240 5P_V12C_2DV2_3P_DCbc9
       1293 5P_V13F_2DV2_3P_DCbc4
       1297 5P_V12C_2DV2_3P_DCbc8
       1305 5P_V15B_2DV5_3P_DCbc4
       1311 5P_V13B_2DV2_3P_DCbc4
       1411 5P_V12C_2DV2_3P_DCbc5
       1633 5P_V11C_2DV2_3P_DCbc4

Some primer combinations were very rare:

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep ">" final.demultiplexed.fasta | cut -d "-" -f1 | cut -d "_" -f 8- | sort | uniq -c | sort -nk1 | head

.. raw:: html

   </div>

Ten least common primer combinations (alphabetically, there were many
single hits)

::

      1 5P_V10A_2DV11_3P_DCbc7
      1 5P_V10A_2DV12_3P_DCbc2
      1 5P_V10A_2DV12_3P_DCbc9
      1 5P_V10A_2DV2_3P_DCbc2
      1 5P_V10A_2GV1_3_3P_GC4bc2
      1 5P_V10A_2GV1_3_3P_GC4bc3
      1 5P_V10A_2GV1_3_3P_GC4bc4
      1 5P_V10A_2GV1_3_3P_GC4bc7
      1 5P_V10A_2GV1_3_3P_GC4bc8
      1 5P_V10A_2GV5_3P_GC1_3bc3
      
      

There is no guarantee that evey primer combination will be found in the
OTU map.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        usearch -otutab final.demultiplexed.fasta -otus combined.1.2.3.90.rehead.fasta -otutabout Finalotutab.txt -mapout Finalmap.txt -notmatched Final.unmapped.fa -sizeout Final.otus_with_sizes.fa

.. raw:: html

   </div>

The map file is a long list of each individual hit -- the number of
identical sequences is still attached to the Read name.

For example, in the table below, the read
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV5\_3P\_DCbc10 had 50 duplicate
copies.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        head Finalmap.txt

.. raw:: html

   </div>

P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV5\_3P\_DCbc10-50 OTU\_0
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV5\_3P\_DCbc10-63 OTU\_6
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV5\_3P\_DCbc10-363 OTU\_0
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV2\_3P\_DCbc10-1 OTU\_1
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV2\_3P\_DCbc10-1 OTU\_1
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV2\_3P\_DCbc10-2 OTU\_1
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV2\_3P\_DCbc10-1 OTU\_1
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV2\_3P\_DCbc10-2 OTU\_5
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV5\_3P\_DCbc10-47 OTU\_0
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV12\_3P\_DCbc10-1 OTU\_59

The map file is a long list of each individual hit -- the number in the
table represents the number of UNIQUE (nonidentical reads) from that
well that matched that OTU. One well, on primer combination, can have
100's of non-identical reads. Many of them are 97% similar, so they get
grouped under the appropriate OTU.

For example, in the table below, column one shows how all the unique
reads were assigned to OTU's for
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV5\_3P\_DCbc10

Well P10A10 with primer set 5P\_V10A\_2DV5\_3P\_DCbc10 (5prime had the
combination of V10A and 2DV5, 3 prime had DCbc10), had 301 UNIQUE reads
that landed in OTU\_0 and 41 different UNIQUE reads that landed in
OTU\_6, etc. Not all the reads were classified as the same OTU, even
with the same primer set.

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        head Finalotutab.txt | less -S

.. raw:: html

   </div>

OTU ID P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV5\_3P\_DCbc10
P10A10\_Vg4\_M\_10\_S\_4\_M\_5P\_V10A\_2DV2\_3P\_DCbc10
P10A10\_Vg4\_M\_10\_S\_4\_M\_ OTU\_0 301 0 0 0 0 0 0 0 0 0 85 0 0 142 0
OTU\_6 41 0 0 0 0 0 0 0 0 0 9 0 0 31 0 OTU\_1 0 4 0 0 0 0 0 0 0 0 1 0 0
0 1 OTU\_5 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 OTU\_59 0 0 1 0 0 0 0 0 0 0 0 0
0 0 0 OTU\_135 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 OTU\_159 5 0 0 0 0 0 0 0 0
0 4 0 0 0 0 OTU\_528 6 0 0 0 0 0 0 0 0 0 2 0 0 1 0 OTU\_262 4 0 0 0 0 0
0 0

Let's look at OTU\_0 and OTU\_6

.. raw:: html

   <div class="sourceCode">

.. code:: bash

        grep "OTU_0\;" Final.otus_with_sizes.fa -A3

.. raw:: html

   </div>

    OTU\_0;size=210417;
    CGATTTTCTGTGAAGCACAGCAAGGCCAACAGAACCTTCCATCTGGTGATCtctcCAGTGAGCCTTGAAGACAGCGCTACTTATTACTGTGCCTCGGGGTATATCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAaaaGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAaaaT

::

    grep "OTU_6\;" Final.otus_with_sizes.fa -A3

    OTU\_6;size=8567;
    CGATTTTCTGTGAAGCACAGCAAGGCCAACAGAACCTTCCATCTGGTGATCtctcCAGTGAGCCTTGAAGACAGCGCTACTTATTACTGTGCCTCGGGGTATCGGAGGGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAaaaGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAaaaT

They are not identical, so the reads must be different. The reads are
assigned to the best match.

Inside the OTU's, it is possible to see how the individual reads differ.

`See picture
here <https://bytebucket.org/arhodes7/mouse_lung_vg4_tcr/raw/809805eea297e1752393461de4f5a9031c68e27c/otu3.47.P10A10.5Prme_V10A_2GV4_3Prime_GC4bc10.png?token=318f350125ff921fd13f5dd3da562307eeba94de>`__

There might be correlations between primer sets and OTU's that could
reduce the complexity of the data set.

To make the data more tractable for excel, I rewrote the column names in
the table to allow separation of the samples by well row, well column,
mouse, sort order and primer sets.

That is what is in your FinalOutput.xls.

`That can be downloaded by clicking this
link. <https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/809805eea297e1752393461de4f5a9031c68e27c/Final.Output.xlsx>`__

(c)2018 Adelaide.Rhodes@uvm.edu
